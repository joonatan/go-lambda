# README

What we are trying to do?

## 1st phase:
- Simple lambda function using Go.
- Gitlab CI/CD pipeline to deploy the lambda function.
- Require lint and tests to pass before deploying.
- Two separate lambda deployment stages: `dev` and `production`.
- `production` deployment should be manual.

## 2nd phase:
- add Turso SQL lite database to the lambda function.
- Require migration to run before deploying the lambda function.

Database:
- Users
- Images

## 3rd phase:
- Download images from S3 bucket.
- Console application to upload images to S3 bucket.
- html page to view images.

Pull all images from S3 container
- paginate them in slices of 20 (or something)

Render basic html template for thumbnails
Add a lightbox for showing the full image

## 4th phase:
- Allow upload through the web interface.
